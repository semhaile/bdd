"""Not quite lisp aoc day 1 2015 feature tests."""
import pytest
from pytest_bdd import (
    given,
    scenario,
    then,
    when,
    parsers
)

from day_one_part_one import p_input

@scenario('features/main_day1.feature', 'Start from floor 0')
def test_start_from_floor_zero():
    """Start from floor zero."""


@given(parsers.parse('We have starting {parenthesis:d}'), target_fixture="challenge_input")
def we_have_starting_parenthesis(parenthesis):
    """We have starting <parenthesis>."""
    return parenthesis


@when('i get instructions in from of open and close parenthesis', target_fixture="floor_result")
def i_get_instructions_in_from_of_open_and_close_parenthesis(challenge_input):
    """i get instructions in from of open and close parenthesis."""
    return p_input(challenge_input)


@then(parsers.parse('I will get a {floor:d}'))
def i_will_get_a_floor(floor_result, floor):
    """I will get a <floor>."""
    assert floor_result == floor
