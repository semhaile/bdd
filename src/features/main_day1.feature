Feature: Not quite lisp aoc day 1 2015
    Help santa find floor in building.

    Scenario Outline: Start from floor 0
        Given We have starting <parenthesis>

        When i get instructions in form of open and close parenthesis # '(' = up  , ')' = down
        
        Then I will get a <floor>

        Examples:
        | parenthesis | floor  |
        | (()         | 1      |
        | ((()        | 2      |
        | )))(((      | 0      |
        | ))))(       | -3     |
        
