""" Aoc day 1 part 1 2015"""

def p_input(parenthesis):
    """Dummy docstring"""
    floor = 0
    for character in parenthesis:
        # check for instructions in form of parenthesis to find correct floor level
        if character == "(":
            floor += 1
        elif character == ")":
            floor -= 1
    return floor

if __name__ == "__main__":
    with open("input_main.txt", encoding="utf-8") as file:
        data = file.read()
    print (p_input(data))
